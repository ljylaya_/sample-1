//
//  NetworkManager.swift
//  Sample 1
//
//  Created by Leah Joy Ylaya on 11/18/20.
//

import Foundation
import UIKit

protocol NetworkManager: AnyObject {
    associatedtype ModelType
    func decode(_ data: Data) -> Photos?
    func load(withCompletion completion: @escaping (Photos?) -> Void)
}

extension NetworkManager {
    fileprivate func load(_ url: URL, withCompletion completion: @escaping (Photos?) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let photo = data else {
                completion(nil)
                return
            }
            completion(self?.decode(photo))
        }
        task.resume()
    }
}

class APIRequest: NetworkManager {
    let methodPath = "/photos/random"
    typealias ModelType = Photo
    var url: URL {
        var components = URLComponents(string: "https://api.unsplash.com")!
        components.path = methodPath
        components.queryItems = queryItems
        return components.url!
    }
    var queryItems: [URLQueryItem] = [
            URLQueryItem(name: "client_id", value: "Df5Hceu4hP3HE4PBSK9UjZdcpB6YtDP4fDo8xNkL_cM"),
            URLQueryItem(name: "orientation", value: "squarish"),
            URLQueryItem(name: "count", value: "10")
        ]
    func decode(_ data: Data) -> Photos? {
        do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(Formatter.iso8601)
            let photos = try decoder.decode(Photos.self, from: data)
            return photos
        } catch {
            debugPrint(error)
            return nil
        }
    }
    
    func load(withCompletion completion: @escaping (Photos?) -> Void) {
        load(url, withCompletion: completion)
    }
}

