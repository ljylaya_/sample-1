//
//  AppUtils.swift
//  Sample 1
//
//  Created by Leah Joy Ylaya on 11/18/20.
//

import Foundation

// formatting date for decoding json, etc.
extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZZ"
//        2020-11-17T05:30:51-05:00
        return formatter
    }()
}
