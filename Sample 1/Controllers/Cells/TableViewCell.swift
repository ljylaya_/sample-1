//
//  TableViewCell.swift
//  Sample 1
//
//  Created by Leah Joy Ylaya on 11/18/20.
//

import UIKit
import Combine

class TableViewCell: UITableViewCell {
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var usernameBottomLabel: UILabel!
    @IBOutlet weak var photoDescription: UILabel!
    private var cancellable: AnyCancellable?
    private var animator: UIViewPropertyAnimator?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        photo.image = #imageLiteral(resourceName: "placeholder")
        animator?.stopAnimation(true)
        cancellable?.cancel()
    }
    
    public func configure(with photo: Photo) {
        usernameLabel.text = photo.user.username
        usernameBottomLabel.text = photo.user.username
        if let desc = photo.photoDescription {
            photoDescription.text = desc
        } else {
            photoDescription.text = ""
        }
        self.photo.updateImageTint()
        likeButton.setImage(photo.likedByUser ? #imageLiteral(resourceName: "heart-shaded") : #imageLiteral(resourceName: "heart-unshaded"), for: .normal)
        cancellable = loadImage(for: photo.urls.regular).sink { [unowned self] image in self.showImage(image: image) }
        cancellable = loadImage(for: photo.user.profileImage.medium).sink { [unowned self] image in self.showProfileImage(image: image) }
    }

    // load images while caching
    private func loadImage(for url: String) -> AnyPublisher<UIImage?, Never> {
        return Just(url)
        .flatMap({ poster -> AnyPublisher<UIImage?, Never> in
            let url =  URL(string: url)!
            return ImageLoader.shared.loadImage(from: url)
        })
        .eraseToAnyPublisher()
    }
    
    private func showImage(image: UIImage?) {
        self.photo.image = image
        animator?.stopAnimation(false)
        animator = UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
        })
    }
    
    private func showProfileImage(image: UIImage?) {
        userProfileImage.image = #imageLiteral(resourceName: "user-placeholder")
        userProfileImage.image = image
    }
    
    private func setupUI() {
        let style = UIScreen.main.traitCollection.userInterfaceStyle
        switch style {
        case .dark:
            usernameLabel.textColor = .white
            usernameBottomLabel.textColor = .white
            likeButton.tintColor = .white
            photoDescription.textColor = .white
            break
        case .light, .unspecified:
            usernameLabel.textColor = .black
            usernameBottomLabel.textColor = .black
            likeButton.tintColor = .black
            photoDescription.textColor = .black
            break
        default:
            break
        }
    }
}
