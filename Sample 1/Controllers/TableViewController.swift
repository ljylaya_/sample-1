//
//  TableViewController.swift
//  Sample 1
//
//  Created by Leah Joy Ylaya on 11/18/20.
//

import UIKit

class TableViewController: UITableViewController {
    private lazy var loading = true
    private var request: AnyObject?
    private var photos = Photos()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = false
        
        // implementing refresh control
        let refController = UIRefreshControl()
        refController.addTarget(self, action: #selector(fetchPhotos(_:)), for: .valueChanged)
        tableView.refreshControl = refController
        fetchPhotos(tableView.refreshControl)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return loading ? 0.0 : UITableView.automaticDimension
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.configure(with: photos[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }

}

private extension TableViewController {
    @objc func fetchPhotos(_ refresh: UIRefreshControl?) {
        refresh?.beginRefreshing()
        let questionRequest = APIRequest()
        request = questionRequest
        questionRequest.load { [weak self] (photos: Photos?) in
            guard let photos = photos else {
                    return
            }
            self?.photos = photos
            self?.loading = false
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                refresh?.endRefreshing()
            }
        }
    }
}
